﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace Gfi_ConceptTest_CRUD1.Data
{
    public class SeedDB
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<ApplicationDbContext>();
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            context.Database.EnsureCreated();
            //ApplicationUser user1 = context.Users.Where(u => u.Email == "test@gfi.com").First();
            //context.Remove(user1);
            //context.SaveChanges();
            if (!context.Users.Any())
            {
                ApplicationUser user = new ApplicationUser()
                {
                    Email = "test@gfi.com",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = "gfi"
                };
                userManager.CreateAsync(user, "Test@1234");
            }
        }
    }
}
