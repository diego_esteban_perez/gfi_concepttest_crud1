﻿using System.Collections.Generic;
using System.Threading.Tasks;

using AutoMapper;

using Gfi_ConceptTest_CRUD1.Entities;
using Gfi_ConceptTest_CRUD1.Models;
using Gfi_ConceptTest_CRUD1.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Gfi_ConceptTest_CRUD1.Controllers
{
    /// <summary>
    /// Authors controller class
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IAuthorRepository authorRepository;

        /// <summary>
        /// Default controller constructor implementing DI
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="authorRepository"></param>
        public AuthorsController(IMapper mapper, IAuthorRepository authorRepository) 
        {
            this.mapper = mapper;
            this.authorRepository = authorRepository;
        }

        // GET: api/Authors
        /// <summary>
        /// Gets a list of authors.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<IEnumerable<AuthorDTO>>> Get()
        {
            List<Author> authors = await authorRepository.GetAuthors();
            if (authors == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "There was a problem getting authors.");
            }

            return mapper.Map<List<AuthorDTO>>(authors);
        }

        // GET: api/Authors/5
        /// <summary>
        /// Gets an author by its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetAuthor")]
        public async Task<ActionResult<AuthorDTO>> Get(int id)
        {
            Author author = await authorRepository.GetAuthor(id);
            if (author.Id == -1)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "There was a problem getting author.");
            }

            if (author == null)
            {
                return NotFound();
            }

            var autorDTO = mapper.Map<AuthorDTO>(author);

            return autorDTO;
        }

        // POST: api/Authors
        /// <summary>
        /// Post a new author.
        /// </summary>
        /// <param name="authorDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] AuthorDTO authorDTO)
        {
            Author author = await authorRepository.Insert(authorDTO);
            if (author == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "There was a problem creating author.");
            }

            return new CreatedAtRouteResult("GetAuthor", new { id = author.Id }, author);
        }

        // PUT: api/Authors/5
        /// <summary>
        /// Update an author.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="author"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] AuthorDTO author)
        {
            if (! await authorRepository.Modify(id, author))
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "There was a problem modifying author.");
            }
            
            return Ok();
        }

        // DELETE: api/Authors/5
        /// <summary>
        /// Removes an author.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (!await authorRepository.Delete(id))
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "There was a problem modifying author.");
            }

            return Ok();
        }
    }
}
