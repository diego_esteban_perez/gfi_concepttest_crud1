using AutoMapper;
using Gfi_ConceptTest_CRUD1.Data;
using Gfi_ConceptTest_CRUD1.Entities;
using Gfi_ConceptTest_CRUD1.Models;
using Gfi_ConceptTest_CRUD1.Repository;
using Gfi_ConceptTest_CRUD1.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using System.IO;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Logging;

namespace Gfi_ConceptTest_CRUD1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddAutoMapper(configuration => 
            {
                configuration.CreateMap<Author, AuthorDTO>();
                configuration.CreateMap<AuthorDTO, Author>();
            }, typeof(Startup));

            services.AddTransient<IAuthorRepository, AuthorRepository>();
            services.AddScoped<AuthorService>();

            services.AddSwaggerGen(config =>
                config.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "Gfi Concept Test CRUD1",
                    Version = "1.00"
                }));
            var xmlFilePath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "Gfi_ConceptTest_CRUD1.xml");
            services.AddSwaggerGen(config =>
                config.IncludeXmlComments(xmlFilePath)
            );

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"))
            );

            var securityKey = Configuration.GetSection("jwt").GetSection("SecurityKey").Value;

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = "http://dotnetdetail.net",
                    ValidIssuer = "http://dotnetdetail.net",
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey))
                };
            });

            //services.AddHttpsRedirection(options =>
            //{
            //    options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
            //    options.HttpsPort = 5001;
            //});

            IdentityModelEventSource.ShowPII = true;

            CreateAuthorsRepository();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile("Logs/Gfi_ConceptTest_CRUD1-{Date}.txt");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            SeedDB.Initialize(app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope().ServiceProvider);

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(config =>
                config.SwaggerEndpoint("/swagger/v1/swagger.json", "Api de ejemplo Swagger")
            );
        }

        private void CreateAuthorsRepository() 
        {
            var authorsFile = Configuration.GetValue<string>("AuthorsFile");
            if (!File.Exists(authorsFile))
            {
                File.Create(authorsFile).Close();
            }
        }
    }
}
