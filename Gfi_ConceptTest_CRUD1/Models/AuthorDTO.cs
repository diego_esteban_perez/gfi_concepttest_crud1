﻿using Gfi_ConceptTest_CRUD1.Services;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Gfi_ConceptTest_CRUD1.Models
{
    public class AuthorDTO
    {
        [Required]
        public string Name { get; set; }
        public int Age { get; set; }
        [JsonConverter(typeof(IsoDateConverter))]
        public DateTime DateOfBirth { get; set; }

        public override string ToString()
        {
            return new StringBuilder().Append(Name).Append(",").Append(Age).Append(",").Append(DateOfBirth.ToShortDateString()).ToString();
        }
    }
}
