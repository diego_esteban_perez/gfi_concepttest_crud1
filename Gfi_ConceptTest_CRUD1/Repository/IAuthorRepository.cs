﻿using Gfi_ConceptTest_CRUD1.Entities;
using Gfi_ConceptTest_CRUD1.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gfi_ConceptTest_CRUD1.Repository
{
    public interface IAuthorRepository
    {
        Task<Author> Insert(AuthorDTO authorDTO);
        Task<Author> GetAuthor(int id);
        Task<List<Author>> GetAuthors();
        Task<bool> Modify(int id, AuthorDTO author);
        Task<bool> Delete(int id);
    }
}
