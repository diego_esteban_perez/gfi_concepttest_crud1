﻿using AutoMapper;
using Gfi_ConceptTest_CRUD1.Entities;
using Gfi_ConceptTest_CRUD1.Models;
using Gfi_ConceptTest_CRUD1.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gfi_ConceptTest_CRUD1.Repository
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        private readonly AuthorService authorService;
        private readonly ILogger logger;
        private readonly string authorsFile;

        public AuthorRepository(IConfiguration configuration, IMapper mapper, AuthorService authorService, ILogger<AuthorRepository> logger)
        {
            this.configuration = configuration;
            this.mapper = mapper;
            this.authorService = authorService;
            this.logger = logger;
            authorsFile = configuration.GetSection("AuthorsFile").Value;
        }

        public async Task<Author> GetAuthor(int id)
        {
            string[] fileContents = null;
            Author author = new Author();

            try
            {
                fileContents = await File.ReadAllLinesAsync(authorsFile);
            }
            catch (IOException ex) //file could be locked
            {
                logger.LogError(ex.Message);
                author.Id = -1;
                return author;
            }

            string line = fileContents.Where(line => line.Split(",")[0] == id.ToString()).FirstOrDefault();
            author = authorService.Line2Author(line);

            return author;
        }

        public async Task<List<Author>> GetAuthors()
        {
            string[] fileContents = null;
            List<Author> authors = null;

            try
            {
                fileContents = await File.ReadAllLinesAsync(authorsFile);
            }
            catch (IOException ex) //file could be locked
            {
                logger.LogError(ex.Message);
                return authors;
            }

            authors = new List<Author>();
            foreach (string line in fileContents)
            {
                Author author = authorService.Line2Author(line);
                authors.Add(author);
            }

            return authors;
        }

        public async Task<Author> Insert(AuthorDTO authorDTO)
        {
            int id = authorService.GetIdFromFile(authorsFile);
            string strAuthor = new StringBuilder().Append(id).Append(",").Append(authorDTO.ToString()).ToString();
            Author author = null;
            try
            {
                string newLine = id > 1 ? Environment.NewLine : "";
                await File.AppendAllTextAsync(authorsFile, newLine + strAuthor);
                author = mapper.Map<Author>(authorDTO);
                author.Id = id;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
            }
            return author;
        }

        public async Task<bool> Modify(int id, AuthorDTO author)
        {
            string[] fileContents = null;
            try
            {
                fileContents = await File.ReadAllLinesAsync(authorsFile);
            }
            catch (IOException ex) //file could be locked
            {
                logger.LogError(ex.Message);
                return false;
            }

            string line = fileContents.Where(line => line.Split(",")[0] == id.ToString()).FirstOrDefault();
            if (line == null)
            {
                return false;
            }

            int index = Array.IndexOf(fileContents, line);
            fileContents[index] = new StringBuilder().Append(id.ToString()).Append(",").Append(authorService.Author2Line(author)).ToString();

            try
            {
                await File.WriteAllLinesAsync(authorsFile, fileContents);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return false;
            }

            return true;
        }

        public async Task<bool> Delete(int id)
        {
            string[] fileContents = null;

            try
            {
                fileContents = await File.ReadAllLinesAsync(authorsFile);
            }
            catch (IOException ex) //file could be locked
            {
                logger.LogError(ex.Message);
                return false;
            }

            string line = fileContents.Where(line => line.Split(",")[0] == id.ToString()).FirstOrDefault();
            if (line == null)
            {
                return false;
            }

            int index = Array.IndexOf(fileContents, line);
            string[] toRemove = { fileContents[index] };
            var result = fileContents.Except(toRemove);

            try
            {
                await File.WriteAllLinesAsync(authorsFile, result);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return false;
            }

            return true;
        }
    }
}
