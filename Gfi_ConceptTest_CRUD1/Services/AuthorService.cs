﻿using Gfi_ConceptTest_CRUD1.Entities;
using Gfi_ConceptTest_CRUD1.Models;
using System;
using System.IO;
using System.Linq;

namespace Gfi_ConceptTest_CRUD1.Services
{
    public class AuthorService
    {
        public int GetIdFromFile(string authorsFile) 
        {
            int id = 1;
            if (File.Exists(authorsFile))
            {
                var lines = File.ReadLines(authorsFile);
                if (lines.Count() > 0) 
                {
                    id = int.Parse(lines.Last().Split(",")[0]) + 1;                
                }
            }
            return id;
        }

        /// <summary>
        /// Extracts an author from an authors text file line
        /// </summary>
        /// <param name="line">Line with comma separated author properties</param>
        /// <returns>Author</returns>
        public Author Line2Author(string line)
        {
            //1,Diego,46,07/08/1973 0:00:00
            string[] authorProperties = line.Split(",");
            if (authorProperties.Count() != 4) 
            {
                return null;
            }
            Author author = new Author
            {
                Id = int.Parse(authorProperties[0]),
                Name = authorProperties[1],
                Age = int.Parse(authorProperties[2]),
                DateOfBirth = DateTime.Parse(authorProperties[3])
            };

            return author;
        }

        /// <summary>
        /// Converts an Author to a authors file line
        /// </summary>
        /// <param name="author">Author</param>
        /// <returns>string</returns>
        public string Author2Line(AuthorDTO author)
        {
            return author.ToString();
        }
    }
}
